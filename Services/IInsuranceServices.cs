﻿using MedicalTest_AAI.Models;

namespace MedicalTest_AAI.Services
{
    public interface IInsuranceServices
    {
        Task<GeneralResponse> validateClaimId(InsuranceClaim claim);

        List<string> validate(string from,InsuranceClaim claim);
    }
}
