﻿using RestSharp;
using MedicalTest_AAI.Constant;
using MedicalTest_AAI.Models;



namespace MedicalTest_AAI.Services
{

  
    public class InsuranceServices : IInsuranceServices
    {
        public async Task<GeneralResponse> validateClaimId(InsuranceClaim claim) {
            GeneralResponse generalResponse = new GeneralResponse();

            var client = new RestClient(GlobalConstants.MockAPI.BASE_URI_SERVICE + GlobalConstants.MockAPI.PATH);
            var request = new RestRequest();

            request.Method = Method.Post;
            request.AddHeader("Accept", GlobalConstants.MockAPI.ACCEPT_HEADER);
            request.AddHeader("Content-Type", GlobalConstants.MockAPI.CONTET_TYPE_HEADER);
            var body = new
            {
                claimId = claim.Id,
                userId = GlobalConstants.MockAPI.USER_ID,
                claimAmount = claim.ClaimAmount
            };
            request.AddJsonBody(body);

            var response = await client.ExecuteAsync<Object>(request);
            var statusCode = response.StatusCode.ToString();

    

            if (statusCode.Equals("OK"))
            {
                generalResponse.message = "";
                generalResponse.status = "Success";
                generalResponse.data = response;
            }
            else {
                generalResponse.message = "Error";
                generalResponse.status = "Failed";
                generalResponse.data = response;
            }

            return generalResponse;
        }

        public List<string> validate(string from,InsuranceClaim claim) {
            var results = new List<string>();

            if (checkNullOrEmpty(claim.Status) == true)
            {
                results.Add("Status is required");
            }

            if (from == "submit")
            {
                // Add conditional validation logic here
                
                if (checkNullOrEmpty(claim.PatientName) == true)
                {
                    results.Add("PatientName is required");
                }

                if (checkNullOrEmpty(claim.DateOfService.ToString()) == true)
                {
                    results.Add("DateOfService is required");
                }

                if (checkNullOrEmpty(claim.MedicalProvider) == true)
                {
                    results.Add("MedicalProvider is required");
                }

                if (checkNullOrEmpty(claim.Diagnosis) == true)
                {
                    results.Add("Diagnosis is required");
                }

                if (checkNullOrEmpty(claim.ClaimAmount.ToString()) == true)
                {
                    results.Add("ClaimAmount is required");
                }
            }

            return results;
        }

        private bool checkNullOrEmpty(string data)
        {

            if (string.IsNullOrEmpty(data))
            {
                return true;
            }

            return false;
        }
    }
}
