﻿namespace MedicalTest_AAI.Constant
{
    public class GlobalConstants
    {
        public static class MockAPI
        {
            public const string BASE_URI_SERVICE = "https://3f2ec1e0-505c-4e63-865a-8d0c04b97671.mock.pstmn.io";

            public const string PATH = "/Validate";

            public const string CONTET_TYPE_HEADER = "application/json";

            public const string ACCEPT_OUTGOING_PAYMENT_HEADER = "*/*";

            public const string ACCEPT_HEADER = "text/plain";

            public const string USER_ID = "67890";

        }
    }
}
