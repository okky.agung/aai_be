﻿using Microsoft.EntityFrameworkCore;
using MedicalTest_AAI.Models;

namespace MedicalTest_AAI.App_Start
{
    public class DBContext : DbContext
    {
        public DBContext(DbContextOptions<DBContext> options) : base(options) { }

        public DbSet<InsuranceClaim> claims { get; set; }
    }
}
