﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace MedicalTest_AAI.Models
{
    public class InsuranceClaim
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(100, ErrorMessage = "Patient name cannot exceed 100 characters")]
        public string PatientName { get; set; }

        public DateTime DateOfService { get; set; }

        [StringLength(100, ErrorMessage = "Medical Provider cannot exceed 100 characters")]
        public string MedicalProvider { get; set; }

        [StringLength(1000, ErrorMessage = "Diagnosis cannot exceed 1000 characters")]
        public string Diagnosis { get; set; }

        [Range(0, 10000, ErrorMessage = "Amount must be between 0 and 10,000")]
        public decimal ClaimAmount { get; set; }

        [StringLength(20, ErrorMessage = "Status cannot exceed 20 characters")]
        public string Status { get; set; }
    }
}
