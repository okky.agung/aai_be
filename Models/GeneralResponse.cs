﻿namespace MedicalTest_AAI.Models
{
    public class GeneralResponse
    {
        public string message { get; set; }
        public string status { get; set; }

        public dynamic data { get; set; }
    }
}
