﻿using MedicalTest_AAI.App_Start;
using MedicalTest_AAI.Models;
using MedicalTest_AAI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MedicalTest_AAI.Controllers
{
   
    [Route("api/[controller]")]
    [ApiController]
    public class InsuranceController : ControllerBase
    {
        private readonly DBContext _context;
        private IInsuranceServices _insuranceService;

        public InsuranceController(DBContext context, IInsuranceServices insuranceService)
        {
            _context = context;
            this._insuranceService = insuranceService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<InsuranceClaim>>> GetClaims([FromQuery] DateTime? startDate,
            [FromQuery] DateTime? endDate)
        {
            var data = _context.claims.AsQueryable();
            
            if (startDate.HasValue)
            {
                data = data.Where(c => c.DateOfService >= startDate.Value);
            }

            if (endDate.HasValue)
            {
                data = data.Where(c => c.DateOfService <= endDate.Value);
            }

            return await data.ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<InsuranceClaim>> GetClaimfindById(int id)
        {
            var claim = await _context.claims.FindAsync(id);

            if (claim == null)
            {
                return NotFound();
            }

            return claim;
        }

        [Route("submit")]
        [HttpPost]
        public async Task<ActionResult<GeneralResponse>> submitClaim(InsuranceClaim claim)
        {
            GeneralResponse generalResponse = new GeneralResponse();

            var results = _insuranceService.validate("submit", claim);

            if (results.Count > 0)
            {
                generalResponse.message = "Error";
                generalResponse.status = "Failed";
                generalResponse.data = results;

                return generalResponse;
            }

            if (!findExist(claim.PatientName,claim.MedicalProvider))
            {

                generalResponse = await _insuranceService.validateClaimId(claim);

                if (generalResponse.status == "Success") {

                    claim.DateOfService = claim.DateOfService.ToUniversalTime();
                    _context.claims.Add(claim);

                    await _context.SaveChangesAsync();
                }

            }
            else
            {

                generalResponse.message = "Duplicate Data Found";
                generalResponse.status = "Failed";

            }
            return generalResponse;

        }

        [Route("update")]
        [HttpPost]
        public async Task<ActionResult<GeneralResponse>> updateClaim(int id, InsuranceClaim claim)
        {
            GeneralResponse generalResponse = new GeneralResponse();

            if (id != claim.Id)
            {
                return BadRequest();
            }

            var results = _insuranceService.validate("update", claim);

            if (results.Count > 0)
            {
                generalResponse.message = "Error";
                generalResponse.status = "Failed";
                generalResponse.data = results;

                return generalResponse;
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var datas = await _context.claims.FindAsync(id);

            if (datas == null)
            {
                return NotFound();
            }

            datas.Status = claim.Status;

            await _context.SaveChangesAsync();

            generalResponse.message = "";
            generalResponse.status = "Success";
            generalResponse.data = new { id = id };

            return generalResponse;

        }

        private bool findExist(string PatientName,string provider)
        {
            return _context.claims.Any(e => e.PatientName == PatientName && e.MedicalProvider == provider);
        }
        
    }
}